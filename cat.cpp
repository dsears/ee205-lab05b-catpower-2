/////////////////////////////////////////////////////////////////////////////
//
/// University of Hawaii, College of Engineering
/// @brief Lab 05b - CatPower 2 - EE 205 - Spr 2022
///
/// @file cat.cpp
/// @version 1.0
///
/// @author Dane Sears dsears@hawaii.edu
/// @date 16_Feb_2022
////////////////////////////////////////////////////////////////////////////

#include "cat.h"

double fromCatPowerToJoule( double catPower ) {
   catPower = 0;
   return catPower;  // Cats do no work
}

double fromJouleToCatPower( double joule ) {
   joule = 0;
   return joule;  // Cats do no work
}
